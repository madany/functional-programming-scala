package recfun

object RecFun extends RecFunInterface:

  def main(args: Array[String]): Unit =
    // println("Pascal's Triangle")
    // for row <- 0 to 10 do
    //   for col <- 0 to row do
    //     print(s"${pascal(col, row)} ")
    //   println()
    print(balance(":-)".toList))

  /**
   * Exercise 1
   */
  def pascal(j: Int, i: Int): Int = 
    if i == j || i == 0 || j == 0 then 1 else pascal(j, i-1) + pascal(j-1, i-1)

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = 
    var cnt = 0
    def traverse(chars: List[Char]): Unit = 
      if chars.isEmpty then return

      if chars.head == ')' then cnt -= 1

      if cnt < 0 then return
      
      if chars.head == '(' then cnt += 1

      traverse(chars.tail)

    traverse(chars)
    cnt == 0

  /**
   * Exercise 3
   */
  def countChange(money: Int, unorderedCoins: List[Int]): Int = 
    def inner_count(money: Int, coins: List[Int]): Int = 
      if money == 0 then return 1
      if coins.isEmpty || coins.head > money then return 0
      
      countChange(money, coins.tail) + countChange(money - coins.head, coins)
      
    inner_count(money, unorderedCoins.sortBy(x => x))