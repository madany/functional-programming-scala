package patmat

abstract class CodeTree
case class Fork(left: CodeTree, right: CodeTree, chars: List[Char], weight: Int) extends CodeTree
case class Leaf(char: Char, weight: Int) extends CodeTree

trait Huffman extends HuffmanInterface:
  def weight(tree: CodeTree): Int = tree match
      case t: Leaf => t.weight
      case t: Fork => t.weight

  def chars(tree: CodeTree): List[Char] = 
    tree match
      case leaf: Leaf => List(leaf.char)
      case fork: Fork => fork.chars

  def makeCodeTree(left: CodeTree, right: CodeTree) =
    Fork(left, right, chars(left) ::: chars(right), weight(left) + weight(right))

  def string2Chars(str: String): List[Char] = str.toList

  def insertIntoTimes(timesList: List[(Char, Int)], char: Char): List[(Char, Int)] = 
    if timesList.isEmpty then List((char, 1))
    else 
      val (curChar, freq) = timesList.head
      if curChar == char then (char, freq+1) :: timesList.tail
      else  timesList.head :: insertIntoTimes(timesList.tail, char)

  def times(chars: List[Char]): List[(Char, Int)] = 
    if chars.isEmpty then List() else insertIntoTimes(times(chars.tail), chars.head)

  def makeOrderedLeafList(freqs: List[(Char, Int)]): List[Leaf] = 
    freqs.sortBy(freq => freq._2).map((ch, freq) => Leaf(ch, freq))

  def singleton(trees: List[CodeTree]): Boolean = !trees.isEmpty && trees.tail.isEmpty

  def insertIntoOrdered[T](lst: List[T], elem: T, key: T => Int): List[T] = 
    lst match
      case Nil => List[T](elem)
      case curElem :: tail if key(elem) < key(curElem) => elem :: curElem :: tail
      case head :: tail => head :: insertIntoOrdered(tail, elem, key)

  def combine(trees: List[CodeTree]): List[CodeTree] = 
    trees match
      case Nil => trees
      case _ :: Nil => trees
      case a :: b :: remTrees => insertIntoOrdered(remTrees, makeCodeTree(a, b), weight)

  def until(done: List[CodeTree] => Boolean, merge: List[CodeTree] => List[CodeTree])(trees: List[CodeTree]): List[CodeTree] = 
    if done(trees) then trees else until(done, merge)(merge(trees))
  def createCodeTree(chars: List[Char]): CodeTree = 
    until(singleton, combine)(makeOrderedLeafList(times(chars))).head

  type Bit = Int

  def decode(root: CodeTree, bits: List[Bit]): List[Char] = 

    def decodeInner(tree: CodeTree, bits: List[Bit]): List[Char] = 
      tree match
        case Leaf(char, _) => char :: decodeInner(root, bits)
        case Fork(left, right, _1, _2) if !bits.isEmpty => 
          if bits.head == 0 then decodeInner(left, bits.tail) else decodeInner(right, bits.tail)
        case _ => List()

    decodeInner(root, bits)
    
  val frenchCode: CodeTree = Fork(Fork(Fork(Leaf('s',121895),Fork(Leaf('d',56269),Fork(Fork(Fork(Leaf('x',5928),Leaf('j',8351),List('x','j'),14279),Leaf('f',16351),List('x','j','f'),30630),Fork(Fork(Fork(Fork(Leaf('z',2093),Fork(Leaf('k',745),Leaf('w',1747),List('k','w'),2492),List('z','k','w'),4585),Leaf('y',4725),List('z','k','w','y'),9310),Leaf('h',11298),List('z','k','w','y','h'),20608),Leaf('q',20889),List('z','k','w','y','h','q'),41497),List('x','j','f','z','k','w','y','h','q'),72127),List('d','x','j','f','z','k','w','y','h','q'),128396),List('s','d','x','j','f','z','k','w','y','h','q'),250291),Fork(Fork(Leaf('o',82762),Leaf('l',83668),List('o','l'),166430),Fork(Fork(Leaf('m',45521),Leaf('p',46335),List('m','p'),91856),Leaf('u',96785),List('m','p','u'),188641),List('o','l','m','p','u'),355071),List('s','d','x','j','f','z','k','w','y','h','q','o','l','m','p','u'),605362),Fork(Fork(Fork(Leaf('r',100500),Fork(Leaf('c',50003),Fork(Leaf('v',24975),Fork(Leaf('g',13288),Leaf('b',13822),List('g','b'),27110),List('v','g','b'),52085),List('c','v','g','b'),102088),List('r','c','v','g','b'),202588),Fork(Leaf('n',108812),Leaf('t',111103),List('n','t'),219915),List('r','c','v','g','b','n','t'),422503),Fork(Leaf('e',225947),Fork(Leaf('i',115465),Leaf('a',117110),List('i','a'),232575),List('e','i','a'),458522),List('r','c','v','g','b','n','t','e','i','a'),881025),List('s','d','x','j','f','z','k','w','y','h','q','o','l','m','p','u','r','c','v','g','b','n','t','e','i','a'),1486387)

  val secret: List[Bit] = List(0,0,1,1,1,0,1,0,1,1,1,0,0,1,1,0,1,0,0,1,1,0,1,0,1,1,0,0,1,1,1,1,1,0,1,0,1,1,0,0,0,0,1,0,1,1,1,0,0,1,0,0,1,0,0,0,1,0,0,0,1,0,1)

  def decodedSecret: List[Char] = decode(frenchCode, secret)

  def encode(tree: CodeTree)(text: List[Char]): List[Bit] = 
    val charSet = text.toSet
    
    def collect(tree: CodeTree, code: List[Bit]): Map[Char, List[Bit]] = 
      tree match
        case Leaf(char, _) => if charSet.contains(char) then Map(char -> code) else Map()
        case Fork(left, right, chars, _) =>
          if chars.exists(charSet.contains) then collect(left, code :+ 0).concat(collect(right, code :+ 1)) else Map()

    val encodeTable = collect(tree, List())

    text.flatMap(c => encodeTable.get(c).get)

  type CodeTable = List[(Char, List[Bit])]

  def codeBits(table: CodeTable)(char: Char): List[Bit] =
     table.find((ch, _) => ch == char).get(1)

  def convert(tree: CodeTree): CodeTable = 
    def build(tree: CodeTree, bits: List[Bit]): CodeTable = 
      tree match
        case Leaf(char, _) => List((char, bits))
        case Fork(left, right, _1, _2) => build(left, bits :+ 0) ::: build(right, bits :+ 1)
    build(tree, List())
      
  def mergeCodeTables(a: CodeTable, b: CodeTable): CodeTable = a:::b

  def quickEncode(tree: CodeTree)(text: List[Char]): List[Bit] = 
    text.flatMap(codeBits(convert(tree)))


object Huffman extends Huffman
