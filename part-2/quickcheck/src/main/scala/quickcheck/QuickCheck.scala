package quickcheck

import org.scalacheck.*
import Arbitrary.*
import Gen.*
import Prop.forAll

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap:
  def genHeap: Gen[H] =
    for
      x <- arbitrary[Int]
      heap <- oneOf(const(empty), genHeap)
    yield insert(x, heap)

  def genEmpty: Gen[H] =
    for
      _ <- arbitrary[Int]
      heap <- const(empty)
    yield heap

  given Arbitrary[H] = Arbitrary(genHeap)

  property("gen1") = forAll { (h: H) =>
    val m = if isEmpty(h) then 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  property("p1/1") = forAll(genEmpty) { (h: H) =>
    findMin(insert(-1, insert(0, h))) == -1
  }

  property("p1/2") = forAll(genEmpty) { (h: H) =>
    findMin(insert(0, insert(-1, h))) == -1
  }

  property("p1/3") = forAll(genEmpty) { (h: H) =>
    findMin(insert(100, insert(1, insert(10, h)))) == 1
  }

  property("p2/1") = forAll(genEmpty) { (h: H) =>
    isEmpty(deleteMin(insert(1, h))) == true
  }

  property("p2/2") = forAll(genEmpty) { (h: H) =>
    isEmpty(deleteMin(deleteMin(insert(1, insert(1, h))))) == true
  }
  property("p2/3") = forAll(genEmpty) { (h: H) =>
    isEmpty(deleteMin(insert(1, insert(1, h)))) == false
  }
  property("p2/4") = forAll(genEmpty) { (h: H) =>
    isEmpty(
      deleteMin(deleteMin(insert(0, deleteMin(insert(-10, insert(10, h))))))
    ) == true
  }

  def flush(h: H): List[Int] =
    if isEmpty(h) then List() else findMin(h) :: flush(deleteMin(h))

  property("p3/1") = forAll { (h: H) =>
    val items = flush(h)
    items.isEmpty || items.zip(items.tail).forall((_ <= _))
  }

  property("p4/1") = forAll { (h1: H, h2: H) =>
    if isEmpty(h1) && isEmpty(h2) then isEmpty(meld(h1, h2))
    else if isEmpty(h1) then findMin(meld(h1, h2)) == findMin(h2)
    else if isEmpty(h2) then findMin(meld(h1, h2)) == findMin(h1)
    else findMin(meld(h1, h2)) == findMin(h1).min(findMin(h2))
  }

  property("px: meld (a,b,..)+(x,y,..)=(a,b,x,y)") = forAll { (h1: H, h2: H) =>
    flush(meld(h1, h2)) == (flush(h1) ++ flush(h2)).sorted
  }

/** 1) If you insert any two elements into an empty heap, finding the minimum of
  * the resulting heap should get the smallest of the two elements back.
  *
  * 2) If you insert an element into an empty heap, then delete the minimum, the
  * resulting heap should be empty.
  *
  * 3) Given any heap, you should get a sorted sequence of elements when
  * continually finding and deleting minima. (Hint: recursion and helper
  * functions are your friends.)
  *
  * 4) Finding a minimum of the melding of any two heaps should return a minimum
  * of one or the other.
  */
