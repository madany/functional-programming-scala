package calculator

enum Expr:
  case Literal(v: Double)
  case Ref(name: String)
  case Plus(a: Expr, b: Expr)
  case Minus(a: Expr, b: Expr)
  case Times(a: Expr, b: Expr)
  case Divide(a: Expr, b: Expr)


extension [K, V](refs: Map[K, V])  
  def exclude(name: K): Map[K, V] = 
    refs.filter((k, _) => k != name)

object Calculator extends CalculatorInterface:
 import Expr.*
  def computeValues(namedExpressions: Map[String, Signal[Expr]]): Map[String, Signal[Double]] =
    namedExpressions.map((name, expSignal) => {
      val valueSignal = Signal {
        expSignal() match
          case Literal(x: Double) => x 
          case plus: Plus => eval(plus, namedExpressions.exclude(name))
          case minus: Minus => eval(minus, namedExpressions.exclude(name))
          case times: Times => eval(times, namedExpressions.exclude(name))
          case divide: Divide => eval(divide, namedExpressions.exclude(name))
          case Ref(rightName) => 
            namedExpressions.get(rightName) match
            case None =>  Double.NaN 
            case Some(exp) => eval(exp(), namedExpressions.exclude(name))
      }
      (name -> valueSignal)
    })
    
  def eval(expr: Expr, references: Map[String, Signal[Expr]])(using Signal.Caller): Double =
    expr match
      case Ref(name) => 
        references.get(name) match
          case None => Double.NaN
          case Some(value) => eval(value(), references)
      case Literal(v) => v
      case Plus(a, b) => eval(a, references) + eval(b, references = references )
      case Minus(a, b) => eval(a, references) - eval(b, references = references )
      case Times(a, b) => eval(a, references) * eval(b, references = references )
      case Divide(a, b) => if eval(b, references) != 0 then eval(a, references) / eval(b, references) else Double.NaN
    

  /** Get the Expr for a referenced variables.
   *  If the variable is not known, returns a literal NaN.
   */
  private def getReferenceExpr(name: String,
      references: Map[String, Signal[Expr]])(using Signal.Caller): Expr =
    references.get(name).fold[Expr] {
      Literal(Double.NaN)
    } { exprSignal =>
      exprSignal()
    }
