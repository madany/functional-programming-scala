package waterPouring

// ----------------
case class Glass(availAmnt: Int, maxAmnt: Int):
  override def toString(): String = s"Glass(${availAmnt}/${maxAmnt})"

type Glasses = Vector[Glass]
case class PathNode(opChain: List[Operation], glasses: Glasses)

enum Operation:
  case Filling(i: Int)
  case Emptying(i: Int)
  case Pouring(i: Int, j: Int)

def generateAllOps(numOfGlasses: Int): List[Operation] =
  val pours = for
    i <- 0 until numOfGlasses
    j <- 0 until numOfGlasses
    if i != j
  yield Operation.Pouring(i, j)
  val fills = (0 until numOfGlasses).map(Operation.Filling.apply)
  val emptys = (0 until numOfGlasses).map(Operation.Emptying.apply)

  (pours ++ fills ++ emptys).toList

def applyOp(op: Operation, glasses: Glasses): Glasses =
  op match
    case Operation.Filling(i) =>
      val g = glasses(i)
      glasses.updated(i, g.copy(availAmnt = g.maxAmnt))

    case Operation.Emptying(i) =>
      val g = glasses(i)
      glasses.updated(i, g.copy(availAmnt = 0))

    case Operation.Pouring(f, t) =>
      val (from, to) = (glasses(f), glasses(t))
      val movedAmount = from.availAmnt.min(to.maxAmnt - to.availAmnt)
      glasses
        .updated(f, from.copy(availAmnt = from.availAmnt - movedAmount))
        .updated(t, to.copy(availAmnt = to.availAmnt + movedAmount))

def getChildNodes(cache: Set[Glasses], ops: List[Operation])(
    node: PathNode
): List[PathNode] =
  ops
    .map(op => PathNode(op :: node.opChain, applyOp(op, node.glasses)))
    .filter(node => !cache.contains(node.glasses))

def pathsList(
    nodes: List[PathNode],
    ops: List[Operation],
    cache: Set[Glasses]
): LazyList[List[PathNode]] =

  val childNodes = nodes.flatMap(getChildNodes(cache, ops))

  childNodes #:: pathsList(childNodes, ops, cache ++ childNodes.map(_.glasses))

// @main
// def main() = {
//   val k = 3
//   val glasses = Vector(Glass(0, 7), Glass(0, 4))
//   val startingNode = PathNode(List(), glasses)

//   val ops = generateAllOps(glasses.length)
//   val ll = pathsList(List(startingNode), ops, Set())
//   val pathNodes = ll.filter(depLst =>
//     depLst.exists(node => node._2.exists(g => g.availAmnt == k))
//   )(0)

//   for node <- pathNodes
//   do println(s"${node.glasses} =>\n\t${node.opChain.reverse}")
// }
