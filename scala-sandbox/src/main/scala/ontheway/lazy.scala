package lz

def infList(i: Int): LazyList[Int] =
  LazyList.cons(i, infList(i + 1))

def opAdd(a: Int, b: Int) =
  println(s"adding ${a} + ${b}")
  a + b

lazy val xs: LazyList[Int] = 0 #:: 1 #:: xs.tail.zip(xs).map(opAdd)
lazy val ys = xs.filter(_ % 2 == 0)
