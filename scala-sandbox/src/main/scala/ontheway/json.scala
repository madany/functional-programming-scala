import javax.lang.model.`type`.NullType

enum Json:
  case Num(v: Int | Double)
  case Bool(v: Boolean)
  case Str(v: String)
  case Seq(v: List[Json])
  case Obj(v: Map[String, Json])
  case Nul

  override def toString(): String =
    this match
      case Num(v)  => s"${v}"
      case Bool(v) => s"${v}"
      case Str(v)  => s"${v}"
      case Seq(v)  => s"${v.map(_.toString()).mkString("[", ", ", "]")}"
      case Obj(v) =>
        s"${v.map((k, v) => s"${k}: ${v.toString()}").mkString("{ ", ", ", " }")}"
      case Nul => "Nul"

object Json:
  def apply(
      x: Int | Double | Boolean | String | List[Json] | Map[String, Json] |
        Json.Nul.type
  ) = x match
    case x: Map[String, Json] => Json.Obj(x)
    case x: List[Json]        => Json.Seq(x)
    case x: Boolean           => Json.Bool(x)
    case x: Int               => Json.Num(x)
    case x: Double            => Json.Num(x)
    case x: String            => Json.Str(x)
    case Json.Nul             => Json.Nul

  // val j = Json(Map("a" -> Json(List(Json(1), Json(false)))))
