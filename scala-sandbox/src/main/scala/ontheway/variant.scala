package variant

trait Integer
case class NonZeroInteger(x: Int) extends Integer:
  require(x != 0)
case class ZeroInteger(x: Int) extends Integer:
  require(x == 0)

trait Generator[+T]:
  def gen(): T

class GeneratorNonZeroInteger extends Generator[NonZeroInteger]:
  def gen() = NonZeroInteger(123)

class GeneratorZeroInteger extends Generator[ZeroInteger]:
  def gen() = ZeroInteger(0)

// val v: Integer = NonZeroInteger(1)

// deleting the `+` will fail the following line:
// val g: Generator[Integer] = GeneratorNonZeroInteger()
