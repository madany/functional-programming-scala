package gens
import scala.util.Random

trait Generator[T]:
  def gen(): T

  def map[U](f: T => U): Generator[U] =
    new Generator[U]:
      def gen() =
        f(Generator.this.gen())

  def flatMap[U](f: T => Generator[U]): Generator[U] =
    new Generator[U]:
      def gen() =
        f(Generator.this.gen()).gen()

class IntGenerator extends Generator[Int]:
  private val r = Random()

  def gen(): Int = r.nextInt()

val intsGen = IntGenerator()
val lensGen = for x <- intsGen yield x.abs % 3
val boolsGen = for x <- intsGen yield x >= 0

def identityGen[T](x: T) = for _ <- intsGen yield x

def listN(n: Int): List[Int] =
  if n == 0 then List()
  else intsGen.gen() :: listN(n - 1)

def listGen: Generator[List[Int]] =
  for n <- lensGen yield listN(n)
