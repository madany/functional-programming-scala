package expr

val TABLE = Map("x" -> 1, "y" -> 2, "z" -> 3)

trait Expr

object Expr:
  // ADTs
  case class Num(x: Int) extends Expr
  case class Id(x: String) extends Expr
  case class Addition(l: Expr, r: Expr) extends Expr
  case class Multiplication(l: Expr, r: Expr) extends Expr

private def showMul(e: Expr): String =
  e match
    case a: Expr.Addition => s"(${show(a)})"
    case other            => s"${show(other)}"

def eval(e: Expr): Int = e match
  case Expr.Num(x)               => x
  case Expr.Id(x)                => TABLE.get(x).get
  case Expr.Addition(a, b)       => eval(a) + eval(b)
  case Expr.Multiplication(a, b) => eval(a) * eval(b)

def show(e: Expr): String =
  e match
    case Expr.Num(x)               => s"${x}"
    case Expr.Id(x)                => s"${x}"
    case Expr.Addition(a, b)       => s"${show(a)} + ${show(b)}"
    case Expr.Multiplication(a, b) => s"${showMul(a)} * ${showMul(b)}"
