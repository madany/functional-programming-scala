// or trait

abstract class IntSet:
  def contains(x: Int): Boolean
  def include(x: Int): IntSet
  def union(s: IntSet): IntSet

object IntSet:
  def apply() = EmptySet
  def apply(x: Int) = EmptySet.include(x)
  def apply(x1: Int, x2: Int) = EmptySet.include(x1).include(x2)

class NonEmptySet(root: Int, left: IntSet, right: IntSet) extends IntSet:

  def contains(x: Int): Boolean =
    if x == root then return true
    if x < root then return left.contains(x)
    right.contains(x)

  def include(x: Int): IntSet =
    if x == root then this
    else if x < root then NonEmptySet(root, left.include(x), right)
    else NonEmptySet(root, left, right.include(x))

  def union(s: IntSet): IntSet =
    if s == EmptySet then this else s.union(left).union(right).include(root)

// class EmptySet extends IntSet:
//   def contains(x: Int) = false
//   def include(x: Int) = NonEmptySet(x, this, this)

object EmptySet extends IntSet:
  def contains(x: Int): Boolean = false
  def include(x: Int): IntSet = NonEmptySet(x, this, EmptySet)

  def union(s: IntSet): IntSet = s
