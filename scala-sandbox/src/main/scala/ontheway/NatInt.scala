trait NatInt:
  val isZero: Boolean
  def prevInt: NatInt
  def nextInt: NatInt
  def +(that: NatInt): NatInt
  def -(that: NatInt): NatInt

object Zero extends NatInt:
  val isZero = true
  def prevInt = ???
  def nextInt = NextInt(this)
  def +(that: NatInt) = that
  def -(that: NatInt) = if that.isZero then this else ???

  override def toString(): String = "Zero"

class NextInt(_prev: NatInt) extends NatInt:
  val isZero = false
  def prevInt = _prev
  def nextInt = NextInt(this)
  def +(that: NatInt) =
    if that.isZero then this else this.nextInt + that.prevInt
  def -(that: NatInt) =
    if that.isZero then this else this.prevInt - that.prevInt

  override def toString(): String = s"Next(${prevInt})"
