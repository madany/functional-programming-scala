class Rational(__num: Int, __den: Int):
  println(s"Creating ${__num}/${__den}")
  require(__num != 1, "Rejecting 1")
  def this(__num: Int) = this(__num, 1)

  require(__den != 0, s"denominator(${__den}) can't be zero")

  private def gcd(a: Int, b: Int): Int =
    if b == 0 then a else gcd(b, a % b)

  private val g = gcd(__num.abs, __den.abs)
  val num = __num.abs / g * (if __num.min(__den) > 0 then +1 else -1)
  val den = __den.abs / g

  def *(that: Rational) =
    Rational(num * that.num, den * that.den)

  def +(that: Rational) =
    Rational(
      num * that.den + that.num * den,
      den * that.den
    )

  def /(that: Rational) =
    Rational(
      this.num * that.den,
      this.den * that.num
    )

  def neg = Rational(-this.num, this.den)

  def -(that: Rational) = this + that.neg

  def <(that: Rational): Boolean = (this.num * that.den) < (this.den * that.num)

  def max(that: Rational) = if this < that then that else this

  override def toString(): String = s"${num}/${den}"

end Rational

extension (r: Rational)
  def abs: Rational = Rational(r.num.abs, r.den)
  infix def min(that: Rational) = if r < that then this else that

// val r = Rational(1, 1)
// println(r min Rational(2, 2)) // r.min(...) <=> r.+(...)
