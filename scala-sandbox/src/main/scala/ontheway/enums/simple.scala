enum Direction(val dx: Int, val dy: Int):
  case Up extends Direction(0, 1)
  case Right extends Direction(1, 0)
  case Down extends Direction(0, -1)
  case Left extends Direction(-1, 0)

  private def at(idx: Int) =
    val m = Direction.values.length
    Direction.values((idx + m) % m)

  def turnClockwise = at(this.ordinal + 1)
  def turnCounterClockwise = at(this.ordinal - 1)

def moveTo(p: (Int, Int), d: Direction) =
  val (x, y) = p
  (x + d.dx, y + d.dy)

// enum Direction:
//   case Up, Right, Down, Left
//   def turnLeft = Direction.values((ordinal + 1) % 4)
